﻿using System;
using System.Collections.Generic;
namespace EGGShell {
    public class EGGParsed {
        public List<Command> Path { get; set; }
        public class Command {
            public string Keyword { get; set; }
            public override string ToString () {
                return string.Format("[Command: Keyword={0}]", Keyword);
            }
        }
    }
}
