﻿using System;
using PEG;
using PEG.SyntaxTree;

namespace EGGShell {
    public class EGGGrammar : Grammar<EGGGrammar> {
        public virtual Expression Start() {
            return Path();
        }
        public virtual Expression Path() {
            return Command() + +(PathSeparator() + Command());
        }
        public virtual Expression Command() {
            return Keyword();
        }
        public virtual Expression Keyword() {
            var alnum = +('A'.To('Z') | 'a'.To('z') | '0'.To('9'));
            // https://github.com/kswoll/npeg/issues/2
            var quotedbase = (-(!('"'._() | @"\") + Peg.Any | @"\\" | @"\"""));
            return alnum | ('"' + quotedbase.Capture() + '"');
        }
        public virtual Expression PathSeparator() {
            return EGGConfig.PathSeparator;
        }
    }
}
