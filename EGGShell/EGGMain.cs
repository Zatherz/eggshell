﻿using System;
namespace EGGShell {
    public static class EGGMain {
        public static void Main() {
            while (true) {
                EGGAST test;
                Console.WriteLine(EGGAST.TryParse(Console.ReadLine(), out test));
                foreach (var command in test.CommandPath) {
                    Console.WriteLine(command);
                }
            }
        }
    }
}
