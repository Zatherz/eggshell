﻿using System;
using System.Collections.Generic;
using PEG;
namespace EGGShell {
    public class EGGAST {
        public List<EGGParsed.Command> CommandPath { get; set; }

        static EGGGrammar grammar = EGGGrammar.Create();
        static PegParser<EGGParsed> parser = new PegParser<EGGParsed>(grammar, grammar.Start());

        public EGGAST(List<EGGParsed.Command> path) {
            CommandPath = path;
        }

        public static bool TryParse(string s, out EGGAST ast) {
            EGGParsed parsed;
            int amountread;
            if (!parser.Parse(s.Trim(), out parsed, out amountread)) {
                ast = default(EGGAST);
                return false;
            }
            ast = new EGGAST(parsed.Path);
            return true;
        }

        public static EGGAST Parse(string s) {
            EGGAST result;
            if (TryParse(s, out result)) {
                return result;
            } else {
                throw new Exception();
            }
        }

        public class EGGASTParserException : Exception {
            public EGGASTParserException(string s) : base("Failed parsing command: " + s) {}
        }
    }
}
